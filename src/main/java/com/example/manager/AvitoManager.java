package com.example.manager;

import com.example.data.Apartment;
import com.example.search.SearchRequest;
import com.example.exception.ItemNotFoundException;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class AvitoManager {
    private static final int SEARCH_SIZE_LIMIT = 10;
    private long nextId = 1;
    private final List<Apartment> items = new ArrayList<>(100);

    public List<Apartment> getAll() {
        return new ArrayList<>(items);
    }

    public Apartment getById(final long id) {
        for (Apartment item : items) {
            if (item.getId() == id) {
                return item;
            }
        }
        throw new ItemNotFoundException("item with id: " + id + " not found");
    }

    public Apartment create(final Apartment item) { // object passed value, but value is reference
        item.setId(nextId++);
        items.add(item);
        log.debug("create apartment, apartment: {}, isnotfirst {}, isnotlast{}", item, item.isNotFirstFloor(), item.isNotLastFloor());
        return item;
    }

    public Apartment update(final Apartment item) {
        final int index = getIndexById(item.getId());
        if (index == -1) {
            throw new ItemNotFoundException("item with id: " + item.getId() + " not found");
        }
        items.set(index, item);
        log.debug("item is updated: {}", item);
        return item;
    }

    public boolean removeById(final long id) {
        log.debug("item with {} is removed", id);
        return items.removeIf(o -> o.getId() == id);
    }

    public List<Apartment> search(final SearchRequest searchRequest) {
        final List<Apartment> results = new ArrayList<>(SEARCH_SIZE_LIMIT);
        for (final Apartment item : items) {
            if (item.matches(searchRequest)) {
                results.add(item);
                if (results.size() >= SEARCH_SIZE_LIMIT) {
                    return results;
                }
            }
        }
        return results;
    }

    public int getCount() {
        return items.size();
    }

    private int getIndexById(final long id) {
        for (int i = 0; i < items.size(); i++) {
            final Apartment apartment = items.get(i);
            if (apartment.getId() == id) {
                return i;
            }
        }
        return -1;
    }
}
