package com.example.util;

public class ApartmentTypes {
    public final static String EXACT = "exact";
    public final static String OPEN_PLAN = "open plan";
    public final static String STUDIO = "studio";
    public final static String[] EXACT_AR= new String[]{EXACT};
    public final static String[] OPEN_PLAN_AR= new String[]{OPEN_PLAN};
    public final static String[] STUDIO_AR= new String[]{STUDIO};
    public final static String[] ALL_ROOM_TYPES= new String[]{EXACT, OPEN_PLAN, STUDIO};
    public final static String[] STUDIO_AND_OPEN= new String[]{STUDIO, OPEN_PLAN};
    public final static String[] STUDIO_AND_EXACT= new String[]{STUDIO, EXACT};
}
