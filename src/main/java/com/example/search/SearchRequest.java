package com.example.search;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SearchRequest {
    private String[] roomType;
    private Integer roomsMin;
    private Integer roomsMax;
    private Integer priceMin;
    private Integer priceMax;
    private Integer areaMin;
    private Integer areaMax;
    private boolean hasBalcony;
    private boolean hasLoggia;
    private Integer floorMin;
    private Integer floorMax;
    private boolean notFirstFloor;
    private boolean notLastFloor;
    private Integer floorsNumberMin;
    private Integer floorsNumberMax;
}
