package com.example.data;

import com.example.search.SearchRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;

import static com.example.util.ApartmentTypes.*;

@Slf4j
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Apartment {
    private long id;
    private String roomType;
    private int rooms = 0;
    private int price;
    private int area;
    private boolean hasBalcony;
    private boolean hasLoggia;
    private int floor;
    private int floorsNumber;

    public boolean isNotFirstFloor() {
        return floor > 1;
    }

    public boolean isNotLastFloor() {
        return floor != floorsNumber;
    }

    public boolean matches(final SearchRequest search) {
        List<String> roomTypes = Arrays.asList(search.getRoomType());
        if (roomType.matches(STUDIO)) {
            if (!roomTypes.contains(STUDIO)) {
                return false;
            }
        }

        if (roomType.matches(OPEN_PLAN)) {
            if (!roomTypes.contains(OPEN_PLAN)) {
                return false;
            }
        }
        if (roomType.matches(EXACT)) {
            if (!roomTypes.contains(EXACT)) {
                return false;
            }
            if (search.getRoomsMin() != null) {
                if (rooms < search.getRoomsMin()) {

                    return false;
                }
            }
            if (search.getRoomsMax() != null) {
                if (rooms > search.getRoomsMax()) {
                    return false;
                }
            }
        }
        if (search.getPriceMin() != null) {
            if (price < search.getPriceMin()) {
                return false;
            }
        }
        if (search.getPriceMax() != null) {
            if (price > search.getPriceMax()) {
                return false;
            }
        }
        if (search.getAreaMin() != null) {
            if (area < search.getAreaMin()) {
                return false;
            }
        }
        if (search.getAreaMax() != null) {
            if (area > search.getAreaMax()) {
                return false;
            }
        }
        if (search.isHasBalcony()) {
            if (!hasBalcony) {
                return false;
            }
        }
        if (search.isHasLoggia()) {
            if (!hasLoggia) {
                return false;
            }
        }
        if (search.getFloorMin() != null) {
            if (floorsNumber < search.getFloorMin()) {
                return false;
            }
            if (floor < search.getFloorMin()) {
                return false;
            }
        }
        if (search.getFloorMax() != null) {
            if (floor > search.getFloorMax()) {
                return false;
            }
        }
        if (search.isNotFirstFloor()) {
            if (!isNotFirstFloor()) {
                return false;
            }
        }
        if (search.isNotLastFloor()) {
            if (!isNotLastFloor()) {
                return false;
            }
        }
        if (search.getFloorsNumberMin() != null) {
            if (floorsNumber < search.getFloorsNumberMin()) {
                return false;
            }
        }
        if (search.getFloorsNumberMax() != null) {
            if (floorsNumber > search.getFloorsNumberMax()) {
                return false;
            }
        }
        log.debug("apartment with id {} fits", id);
        return true;
    }
}
