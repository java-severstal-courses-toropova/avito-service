package com.example.manager;

import com.example.data.Apartment;
import com.example.search.SearchRequest;
import com.example.exception.ItemNotFoundException;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.util.ArrayList;
import java.util.List;

import static com.example.util.ApartmentTypes.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AvitoManagerTest {
    static AvitoManager manager = new AvitoManager();
    static Apartment apOpenPlan = new Apartment(0, OPEN_PLAN, 0, 25_000_000, 100_00, true, false, 15, 17);
    static Apartment studioWithBalcony = new Apartment(0, STUDIO, 0, 5_000_000, 30_00, true, false, 5, 6);
    static Apartment ap3RoomsWithLoggiaFirstFloor = new Apartment(0, EXACT, 3, 15_500_000, 70_00, false, true, 1, 10);
    static Apartment studioWithLoggiaLastFloor = new Apartment(0, STUDIO, 0, 8_000_000, 40_00, false, true, 10, 10);
    static Apartment ap4RoomsWithBalconyAndLoggia = new Apartment(0, EXACT, 3, 18_000_000, 70_00, true, true, 1, 4);

    @Test
    @Order(1)
    void shouldCreate() {
        List<Apartment> expectedAll = new ArrayList<>();
        List<Apartment> actualAll = manager.getAll();
        assertEquals(expectedAll, actualAll);
        assertEquals(0, manager.getCount());
    }

    @Test
    @Order(2)
    void shouldAddSingle() {
        int notExpectedId = 0;
        Apartment actualCreate = manager.create(apOpenPlan);
        assertNotEquals(notExpectedId, actualCreate.getId());

        List<Apartment> expectedAll = new ArrayList<>();
        apOpenPlan.setId(1);
        expectedAll.add(apOpenPlan);
        List<Apartment> actualAll = manager.getAll();
        assertEquals(expectedAll, actualAll);

        Apartment actualById = manager.getById(actualCreate.getId());
        assertNotNull(actualById);

        assertEquals(1, manager.getCount());
    }

    @Test
    @Order(3)
    void shouldAddMultiple() {
        int notExpectedId = 0;
        Apartment actualCreate = manager.create(studioWithBalcony);
        assertNotEquals(notExpectedId, actualCreate.getId());

        List<Apartment> expectedAll = new ArrayList<>();
        expectedAll.add(apOpenPlan);
        studioWithBalcony.setId(2);
        expectedAll.add(studioWithBalcony);
        List<Apartment> actualAll = manager.getAll();
        assertEquals(expectedAll, actualAll);

        Apartment actualById = manager.getById(actualCreate.getId());
        assertNotNull(actualById);

        assertEquals(2, manager.getCount());
    }

    @Test
    @Order(4)
    void shouldUpdateExisting() {
        studioWithBalcony.setPrice(4_500_000);
        Apartment expected = studioWithBalcony;
        Apartment actual = manager.update(studioWithBalcony);
        assertEquals(expected, actual);
    }

    @Test
    @Order(5)
    void shouldUpdateNotExisting() {
        assertThrows(ItemNotFoundException.class, () -> manager.update(studioWithLoggiaLastFloor));
    }

    @Test
    @Order(6)
    void shouldGetByIdExisting() {
        Apartment expected = studioWithBalcony;
        Apartment actual = manager.getById(2);
        assertEquals(expected, actual);
    }

    @Test
    @Order(7)
    void shouldGetByIdNotExisting() {
        assertThrows(ItemNotFoundException.class, () -> manager.getById(1000));
    }

    @Test
    @Order(8)
    void shouldRemoveByIdIfExists() {
        manager.removeById(1);
        List<Apartment> expected = new ArrayList<>();
        expected.add(studioWithBalcony);
        List<Apartment> actual = manager.getAll();

        assertEquals(expected, actual);
        assertEquals(1, manager.getCount());
    }

    @Test
    @Order(9)
    void shouldRemoveRemoved() {
        assertFalse(manager.removeById(1));
    }

    @Test
    @Order(10)
    void shouldRemoveNotExisting() {
        assertFalse(manager.removeById(1000));
    }

    @Test
    @Order(11)
    void shouldSearchNoResults() {
        List<Apartment> actual = manager.search(
                new SearchRequest(EXACT_AR, 4, 5, null, null, null, null, false, false, null, null, false, false, null, null));
        assertEquals(0, actual.size());
    }

    @Test
    @Order(12)
    void shouldSingleResult() {
        manager.create(ap3RoomsWithLoggiaFirstFloor);
        ap3RoomsWithLoggiaFirstFloor.setId(3);
        List<Apartment> actual = manager.search(
                new SearchRequest(EXACT_AR, 2, 5, null, null, null, null, false, true, null, null, false, true, null, null));
        assertEquals(1, actual.size());
    }

    @Test
    @Order(13)
    void shouldResultAll() {
        manager.create(studioWithLoggiaLastFloor);
        studioWithLoggiaLastFloor.setId(4);
        List<Apartment> actual = manager.search(
                new SearchRequest(ALL_ROOM_TYPES, null, null, null, null, null, null, false, false, null, null, false, false, null, null));
        assertEquals(manager.getCount(), actual.size());
    }

    @Test
    @Order(14)
    void shouldSearchOpenPlan() {
        apOpenPlan.setId(0);
        manager.create(apOpenPlan);
        apOpenPlan.setId(5);
        List<Apartment> actual = manager.search(
                new SearchRequest(OPEN_PLAN_AR, null, null, null, null, null, null, false, false, null, null, false, false, null, null));
        assertEquals(apOpenPlan, actual.get(0));
    }

    @Test
    @Order(15)
    void shouldSearchStudio() {
        List<Apartment> actual = manager.search(
                new SearchRequest(STUDIO_AR, null, null, null, null, null, null, false, false, null, null, false, false, null, null));
        assertEquals(2, actual.size());
    }

    @Test
    @Order(16)
    void shouldSearchExactRoomsAndStudio() {
        List<Apartment> actual = manager.search(
                new SearchRequest(STUDIO_AND_EXACT, 3, 3, null, null, null, null, false, false, null, null, false, false, null, null));
        assertEquals(3, actual.size());
    }

    @Test
    @Order(17)
    void shouldSearchOpenPlanAndStudio() {
        List<Apartment> actual = manager.search(
                new SearchRequest(STUDIO_AND_OPEN, null, null, null, null, null, null, false, false, null, null, false, false, null, null));
        assertEquals(3, actual.size());
    }

    @Test
    @Order(18)
    void shouldSearchOpenPlanAndStudioWithBalcony() {
        List<Apartment> actual = manager.search(
                new SearchRequest(STUDIO_AND_OPEN, null, null, null, null, null, null, true, false, null, null, false, false, null, null));
        assertEquals(2, actual.size());
    }

    @Test
    @Order(19)
    void shouldSearchExactByAllParamsSingleResult() {
        List<Apartment> actual = manager.search(
                new SearchRequest(EXACT_AR, 2, 5, 1_000_000, 30_000_000, 50_00, 200_00, false, true, 1, 10, false, true, 5, 25));
        assertEquals(ap3RoomsWithLoggiaFirstFloor, actual.get(0));
    }

    @Test
    @Order(20)
    void shouldSearchExactByAllParamsMultipleResult() {
        manager.create(ap4RoomsWithBalconyAndLoggia);
        ap4RoomsWithBalconyAndLoggia.setId(6);
        List<Apartment> actual = manager.search(
                new SearchRequest(EXACT_AR, 2, 5, 1_000_000, 30_000_000, 50_00, 200_00, false, false, 1, 10, false, true, 4, 25));
        assertEquals(2, actual.size());
    }

    @Test
    @Order(21)
    void shouldSearchWithLoggia() {
        List<Apartment> actual = manager.search(
                new SearchRequest(ALL_ROOM_TYPES, null, null, 1_000_000, 30_000_000, null, 200_00, false, true, 1, null, false, false, null, null));
        assertEquals(3, actual.size());
    }

    @Test
    @Order(22)
    void shouldSearchWithBalcony() {
        List<Apartment> actual = manager.search(
                new SearchRequest(ALL_ROOM_TYPES, null, null, 1_000_000, 30_000_000, null, 200_00, true, false, 1, null, false, false, null, null));
        assertEquals(3, actual.size());
    }

    @Test
    @Order(23)
    void shouldSearchWithBalconyAndLoggia() {
        List<Apartment> actual = manager.search(
                new SearchRequest(ALL_ROOM_TYPES, null, null, 1_000_000, 30_000_000, null, 200_00, true, true, 1, null, false, false, null, null));
        assertEquals(ap4RoomsWithBalconyAndLoggia, actual.get(0));
    }

    @Test
    @Order(24)
    void shouldSearchByArea() {
        List<Apartment> actual = manager.search(
                new SearchRequest(ALL_ROOM_TYPES, null, null, 1_000_000, 30_000_000, 50_00, 70_00, false, false, 1, null, false, false, null, null));
        assertEquals(2, actual.size());
    }

    @Test
    @Order(25)
    void shouldSearchNotFirstFloor() {
        List<Apartment> actual = manager.search(
                new SearchRequest(ALL_ROOM_TYPES, null, null, 1_000_000, 30_000_000, null, null, false, false, null, null, true, false, null, null));
        assertEquals(3, actual.size());
    }

    @Test
    @Order(26)
    void shouldSearchNotLastFloor() {
        List<Apartment> actual = manager.search(
                new SearchRequest(ALL_ROOM_TYPES, null, null, 1_000_000, 30_000_000, null, null, false, false, null, null, false, true, null, null));
        assertEquals(4, actual.size());
    }

    @Test
    @Order(27)
    void shouldSearchByFloors() {
        List<Apartment> actual = manager.search(
                new SearchRequest(ALL_ROOM_TYPES, null, null, 1_000_000, 30_000_000, null, null, false, false, 5, 10, false, false, null, null));
        assertEquals(2, actual.size());
    }

    @Test
    @Order(28)
    void shouldSearchByPrice() {
        List<Apartment> actual = manager.search(
                new SearchRequest(ALL_ROOM_TYPES, null, null, 5_000_000, 10_000_000, null, null, false, false, null, null, false, false, null, null));
        assertEquals(studioWithLoggiaLastFloor, actual.get(0));
        assertEquals(1, actual.size());
    }

    @Test
    @Order(29)
    void shouldSearchByPriceMax() {
        List<Apartment> actual = manager.search(
                new SearchRequest(ALL_ROOM_TYPES, null, null, null, 20_000_000, null, null, false, false, null, null, false, false, null, null));
        assertEquals(4, actual.size());
    }

    @Test
    @Order(30)
    void shouldSearchByFloorsNumber() {
        List<Apartment> actual = manager.search(
                new SearchRequest(ALL_ROOM_TYPES, null, null, null, 40_000_000, null, null, false, false, null, null, false, false, 10, 12));
        assertEquals(2, actual.size());
    }

    @Test
    @Order(31)
    void shouldSearchMoreThanLimitResults() {
        Apartment ap7 = new Apartment(0, EXACT, 5, 21_000_000, 90_00, true, false, 4, 4);
        manager.create(ap7);
        manager.create(ap7);
        manager.create(ap7);
        manager.create(ap7);
        manager.create(ap7);
        manager.create(ap7);
        manager.create(ap7);
        manager.create(ap7);
        manager.create(ap7);
        manager.create(ap7);
        manager.create(ap7);
        manager.create(ap7);
        List<Apartment> actual = manager.search(
                new SearchRequest(EXACT_AR, 5, 5, null, 40_000_000, null, null, true, false, null, null, false, false, null, 12));
        assertEquals(10, actual.size());
    }
}
